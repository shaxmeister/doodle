function amigoRow(params){
    
  var row = Ti.UI.createTableViewRow({
    className: 'row',
    height: 50,
    title:params.Vch_UserName,
    backgroundImage:'Images/off_4.png',
        selectedBackgroundImage:'Images/on_4.png',
        backgroundColor:'transparent',
        color:'transparent',
        selectedColor : 'white',
        idpp:params.ID__Usuario
  });
  var Lbl = Ti.UI.createLabel({
    text:params.Vch_UserName,
    font:{fontSize:13 ,fontWeight:'bold'},
    height:40,
    width:Ti.UI.SIZE,
    color:'navy',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    left:54 
    });


var Img = Ti.UI.createImageView({
    top:2,
    left:2,
    width:46,
    height:46,
    image:params.Vch_Foto
});
  
  
  row.add(Img);
  row.add(Lbl);
  return row;
    
}

exports.tableAmigos = function(id, url, tv){
    Ti.API.info("get amighos");
    tv.setData([ ]);
 var client = Ti.Network.createHTTPClient({
     // Procesamos la info
     onload : function(e) {
         
         Ti.API.info("AMIGOS Received text: " + this.responseText);
         var res = JSON.parse(this.responseText);
         for(var i=0;i<res.Amigos.length;i++){
             var row = amigoRow(res.Amigos[i]);
             tv.appendRow(row);
         }
     },
     onerror : function(e) {
         Ti.API.debug(e.error);
         alert('error');
     },
     timeout : 5000 
 });
 client.open("POST", url);
 client.send({
     'Num_Tipo' : 4,
     'ID__Usuario':id
 }); 
    
    
};

