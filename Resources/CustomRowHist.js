var CustomRow = function(/*Object*/ _params) {

var row = Ti.UI.createTableViewRow({
		backgroundImage:'Images/off_1.png',
		selectedBackgroundImage:'Images/on_1.png',
		backgroundColor:'transparent',
		color:'transparent',
		selectedColor : 'transparent',
		height:50,
		title:_params.Vch_UserName,
		className:'tablaJJ'
	});
	
	var rowImage = Ti.UI.createImageView({
		image:_params.Vch_Foto,
		height:34,
		width:64,
		top:8,
		left:2,
	});
	row.add(rowImage);
	
	var primaryLabel = Ti.UI.createLabel({
		text:_params.Vch_UserName,
		font:{
			fontSize:16,
			fontWeight:'normal'
		},
		color:'#003333',
		left:75
	});
	row.add(primaryLabel);
	
	var rowImage2 = Ti.UI.createImageView({
		image:'Iconos/IconJG2png.png',
		height:29,
		width:33,
		top:10,
		right:27, 
	});
	row.add(rowImage2);
	
	var secondaryLabel = Ti.UI.createLabel({
		text:_params.Num_Ganados,
		font:{
			fontSize:14,
			fontWeight:'normal'
		},
		color:'#000066',
		right:39, top: 13
	});
	
	row.add(secondaryLabel);
	return row;
};

module.exports = CustomRow;