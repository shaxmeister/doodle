
///	Crear Alerta personalizada	///
function customAlertGanadorVirtual()
{

	// Create a Button.
	var okButton = Ti.UI.createButton({
		title : '   Ver',
		backgroundImage:'Images/BtnAceptar.png',
		height : 36,
		color:'#fff',
		font:{
			fontSize:14,
			fontWeight:'normal'
		},
		width : 110,
		left : 14,
		bottom: 18
	});
	
	// Listen for click events.
	okButton.addEventListener('click', function() {
		view.hide();
	});
	
	
	// Create a Button.
	var cancelButton = Ti.UI.createButton({
		backgroundImage:'Images/BtnRechazar.png',
		title : '     Cancelar',
		color:'#fff',
		font:{
			fontSize:14,
			fontWeight:'normal'
		},
		height : 36,
		width : 110,
		right : 14,
		bottom: 18
	});
	
	// Listen for click events.
	cancelButton.addEventListener('click', function() {
		view.hide();
	});
	
	
	// Create a Label.
	var texto = Ti.UI.createLabel({
		text : ' ¡¡Ganador Partida 1!! \n Vidas Ganadas: 1 \n ¿Deseas Visualizar los Trazos?',
		color: '#000',
		font : {fontSize:15},
		width : Ti.UI.SIZE,
		top : 25,
		left : 21,
		textAlign : 'center'
	});
	
	
	// Create an ImageView.
	var iv = Ti.UI.createImageView({
		image : 'Iconos/Vidas.png',
		width : 31,
		height : 36,
		top : 26,
		left : 27
	});
	
	iv.addEventListener('load', function() {
		Ti.API.info('Image loaded!');
	});

	
	var t = Titanium.UI.create2DMatrix().scale(0);
	var view = Titanium.UI.createView({
	backgroundImage:'Images/FondoAlert.png',
  	borderWidth:3,
  	borderColor:'#fff',
 	height:150,
 	width:260,
  	borderRadius:20,
	opacity:0.92,
	transform:t
	});


// create first transform to go beyond normal size
var t1 = Titanium.UI.create2DMatrix().scale(1.3);

var a = Titanium.UI.createAnimation();
a.transform = t1;
a.duration = 600;

// when this animation completes, scale to normal size
a.addEventListener('complete', function()
{
    // we can use the identity transform to take it back to it's real size
  var t2 = Titanium.UI.create2DMatrix();
  view.animate({transform:t2, duration:200});
});

// Add to the parent view.
	view.add(okButton);
	view.add(cancelButton);
	view.add(texto);
	view.add(iv);

function anim(e){
	view.removeEventListener('postlayout', anim);
	view.animate(a);
	Ti.API.info('postlayout')
}

view.addEventListener('postlayout', anim);

return view;
}

module.exports = customAlertGanadorVirtual;