
 var hei=Ti.Platform.displayCaps.platformHeight;
   var wid=Ti.Platform.displayCaps.platformWidth;
exports.screenHeight = hei;
exports.screenWidth = wid;

exports.getImage = function(successFunction){
    
    var opts = {
            title: 'Selecciona fuente',
            options : ['Cámara', 'Librería']
        };
        
             var dialog = Ti.UI.createOptionDialog(opts);
             
            dialog.addEventListener('click',function(e){
                Ti.API.info(JSON.stringify(e));
                
                if(e.index==0){
                    
                    Ti.Media.showCamera({
                        mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO],
                        success: function(m){
                                  successFunction(m.media);
                                }
                    });
                }else{
                 
                Ti.Media.openPhotoGallery({
                    mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO],
                    success: function(m){
                                  successFunction(m.media);
                                }
                });

                }
            });
            dialog.show();
};

/*
 * data: datos para el post en formato objeto
 * url
 * callback: para el onload
 */
exports.httprequest = function(params){
    Ti.API.info(params.url);
    Ti.API.info(params.data);
 var client = Ti.Network.createHTTPClient({
     onload : function(e) {
         Ti.API.info("Received text: " + this.responseText);
         params.callback(this.responseText);
     },
     onerror : function(e) {
         Ti.API.error(JSON.stringify(e));
         alert('Error de conexion.');
     },
     timeout : 50000  // in milliseconds
 });
 // Prepare the connection.
 client.open("POST", params.url);
 // Send the request.
 client.send(params.data); 
};
