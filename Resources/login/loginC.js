
exports.validate = function(data){
    var valor = data.value;
    if(valor.length>15){
      alert('No puede ser mayor a 15 caracteres');
      data.source.value='';  
    }
};

exports.removeAllSpaces = function(str){
    return str.replace(/\s/g, "") ;
};

exports.sendData = function(data){
    Ti.API.info(data);
   var config = require('utils/config'); 
   //// Si el usuario ya existe, preguntar si es el, si no que ponga otro username
           var opts = {
            title: 'Este usuario ya existe, ¿eres tu?'
        };
        opts.options = ['Si', 'No'];
             var dialog = Ti.UI.createOptionDialog(opts);
            dialog.addEventListener('click',function(e){
                Ti.API.info(JSON.stringify(e));
                
                if(e.index==0){
             Ti.App.Properties.setString('id', dialog.res.id);
             Ti.App.Properties.setString('imagen', dialog.res.imagen);
             Ti.App.Properties.setString('username', data.username);
                    data.run(); 
                }else{
                   alert("Escoge otro usuario."); 
                }
                
                
            });
            
  
  var url = ""+config.sitio+"Crear_Usuario.aspx";
 var client = Ti.Network.createHTTPClient({
     // function called when the response data is available
     onload : function(e) {
         Ti.API.info("Received text: " + this.responseText);
         var res = JSON.parse(this.responseText);
         if(res.retorno==0){
             dialog.res=res;
            dialog.show();
         }else{
             Ti.App.Properties.setString('id', res.id);
             Ti.App.Properties.setString('imagen', res.imagen);
             Ti.App.Properties.setString('username', data.username);
          data.run();   
         }
         
     },
     // function called when an error occurs, including a timeout
     onerror : function(e) {
         Ti.API.debug(e.error);
     },
     timeout : 7000  // in milliseconds
 });
 // Prepare the connection.
 client.open("POST", url);
 // Send the request.
 client.send({
     Vch_UserName:data.username,
     Vch_Correo : data.email,
     Vch_Foto : data.dp
 }); 
  
  
   //data.run(); 
};


///  obtener imagen perfil
exports.getPicture = function(blobPic,setTitle){
  
    
           var opts = {
            title: 'Selecciona fuente',
            options : ['Cámara', 'Librería']
        };
        
             var dialog = Ti.UI.createOptionDialog(opts);
             
            dialog.addEventListener('click',function(e){
                Ti.API.info(JSON.stringify(e));
                
                if(e.index==0){
                    
                    Ti.Media.showCamera({
                        mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO],
                        success: function(e){
                                  blobPic=e.media;
                                  setTitle('Foto cargada');
                                }
                    });
                }else{
                 
                Ti.Media.openPhotoGallery({
                    mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO],
                    success: function(e){
                              blobPic=e.media;
                              setTitle('Foto cargada');
                            }
                });

                }
            });
            dialog.show();
    
};
