
/*
 * Ventana de login
 */

function loginUI(runGame){
    
    var controller = require('login/loginC');
    var dp;
    
    var window = Ti.UI.createWindow({
        title:'Registro',
        backgroundColor:'white',
        layout:'vertical',
        navBarHidden:false
    });
    
    var textField = Ti.UI.createTextField({
      borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
      top: 30,
      autocorrect:false,
      width: 250, height: 44
    });
    textField.addEventListener('blur',controller.validate);
    
    var emailTf = Ti.UI.createTextField({
      borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
      top: 30,
      hintText:'e-mail opcional',
      keyboardType : Ti.UI.KEYBOARD_EMAIL,
      autocorrect:false,
      width: 250, height: 44
    });
    
    var instruccion = Ti.UI.createLabel({
  text: 'Ingresa el nombre de usuario con el que desees identificarte.',
  textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
  top: 30,
  width: Ti.UI.SIZE, height: Ti.UI.SIZE
    });
    
    var aceptarButton = Titanium.UI.createButton({
   title: 'Aceptar',
   top: 30,
   width: 120,
   height: 50
    });
    aceptarButton.addEventListener('click',function(){
        
        var un = controller.removeAllSpaces(textField.value);
        if(un!=""){
             controller.sendData({
           'username' : textField.value,
           'email' : emailTf.value,
           'dp' : dp,
           'run' : function(){
               window.close();
              // setTimeout(function(){window.close();},1000);
               setTimeout(function(){runGame();},500);
               }
            });
        }else{
            alert("Nombre de usuario vacio.");
        }
        
        
    });
    
    var cargarFoto = Ti.UI.createButton({
        title:'Cargar foto perfil(opcional)',
        width:220,
        height:38,
        top:15
    });
    cargarFoto.addEventListener('click',function(){
        controller.getPicture(dp,cargarFoto.setTitle);
    });
    
    
    window.add(instruccion);
    window.add(textField);
    window.add(emailTf);
    window.add(cargarFoto);
    window.add(aceptarButton);
    
    
    return window;
    
}

module.exports = loginUI;
