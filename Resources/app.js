// this sets the background color of the master UIView (when there are no windows/tab groups on it)
Titanium.UI.setBackgroundImage('FondoDefault.png');



var loginObj = require('login/loginUI');
var config = require('utils/config');
if(Titanium.App.Properties.hasProperty('id')){
    initGame();
}else{
    var loginW = new loginObj(initGame);
loginW.open();
}


function initGame(){
    
    config.registerPN();
    var tools = require('utils/tools');
    var idpp = Ti.App.Properties.getString('id');
    var imagenpp = Ti.App.Properties.getString('imagen');
    var usernamepp = Ti.App.Properties.getString('username');
    var idJuego, imagenPCopiar='';
  
    var CustomRow = require('CustomRow');
    var platform = (Titanium.Platform.getOsname()=='ipad');
      Ti.API.info("id "+idpp+" img: "+imagenpp+" Platform ipad? "+platform );
// CREACIÓN DE TABGROUP
var tabGroup = Titanium.UI.createTabGroup({
	barColor:'#006666'
});
//
// ****************************************************VENTANA JUEGOS**************************************************************
//

		var closeJuegoButton = Ti.UI.createButton({
        top:0,
        left:0,
        width:120,
        height:38,
        title:'Cerrar'
        });
        closeJuegoButton.addEventListener('click',function(){
            winJuegoII.close();
        });
		var winJuegoII = Titanium.UI.createWindow({
		    		title:'Juego',
		    		barColor: 'none',
				    backgroundImage:'Images/JuegoII.png',
				    rightNavButton:closeJuegoButton
					});
		winJuegoII.addEventListener('close',function(){
		    aEnviar.setTitle('  Empezar el juego!');
		    segundosRestantes=15;
		    aEnviar.removeEventListener('click',aEnviarDibujo);
		    aEnviar.addEventListener('click',aEnviarListener);
		    aLabelCr.setText(''+segundosRestantes+' Seg');
		});			
					
					// Create an ImageView.
					var anImageViewJ = Ti.UI.createImageView({
						image:imagenpp,
						height:40,
						width:40,
						top:15,
						left:25
					});
					
					// Create a Label.
					var aLabelJ = Ti.UI.createLabel({
						text : usernamepp,
						color : '#fff',
						font : {fontSize:16, fontFamily:'Myriad Pro', fontWeight:'normal'},
						top : 24,	
						left: 70,	
						textAlign : 'left'	
					});
					var segundosRestantes=15;
					var aLabelCr = Ti.UI.createLabel({
						text : '15 Seg',
						color : '#fff',
						font : {fontSize:16, fontFamily:'Myriad Pro', fontWeight:'normal'},
						top : 24,	
						right: 15,	
						textAlign : 'left'	
					});
					var countD;
					function countDown(){
					    segundosRestantes--;
					    aLabelCr.setText(''+segundosRestantes+' Seg');
					    if(segundosRestantes==0){
					        wv.setTouchEnabled(false);
					    clearInterval(countD);
					    }
					}
					
            	//////////////////////////CANVAS//////////////////////////
            	Ti.API.info('heght: '+ (tools.screenHeight*0.6)+' width: '+(tools.screenWidth*0.9));
            	
            	var wv = Ti.UI.createWebView({
                    url : (platform ? 'paintIpad.html' : 'paint.html'),
                    borderWidth:0,
                    height: (tools.screenWidth*0.9),
                    width: (tools.screenWidth*0.9),
                    touchEnabled : false
                });
				
					
					
					// Create a Button.
					var aEnviar = Ti.UI.createButton({
						backgroundImage:'Images/BtnSenDib.png',
						title : '  Empezar el juego!',
						font:{fontSize:16,fontFamily:'Myriad Pro',color:'#006666'},
						textAlign:'center',
						height : 36,
						width : 210,
						bottom : 15
					});
					
					// Listen for click events.
					function aEnviarListener() {
					    aEnviar.removeEventListener('click',aEnviarListener);
					    aEnviar.addEventListener('click',aEnviarDibujo);
					    aEnviar.setTitle(' Enviar Dibujo');
                        wv.evalJS("changeColor('"+viewColor.getBackgroundColor()+"');"); 
                        wv.setTouchEnabled(true);
                       countD = setInterval(countDown,1000);
                        // var MyClass = require ('customAlertEnvio');
                        // var ca = MyClass();
                        // winJuegoII.add(ca);
                    }
                    function aEnviarDibujo() {
                        aEnviar.setTitle(' Enviando');
                        Ti.API.info("id juego "+idJuego);
                        wv.setTouchEnabled(false);
                       
                           tools.httprequest({
                           url:(""+config.sitio+"Juegos_Espera.aspx"),
                           data:{
                               Num_Tipo:3,
                               ID__Juego:idJuego,
                              Vch_Color : (viewColor.getBackgroundColor()),
                               Vch_Imagen:wv.toImage()
                           },
                           callback:function(resp){
                               resp = JSON.parse(resp);
                               if(resp.retorno>0){
                                   segundosRestantes=15;
                                   clearInterval(countD);
                                    winJuegoII.close();
                                   setTimeout(function(){
                                       winJuegoI.close();
                                       oRows.tableJuegos(idpp,(""+config.sitio+"Juegos_Espera.aspx"),table1, idpp);
                                       },500);
                                if(resp.retorno==2){
                                  setTimeout(function(){
                                        //var gw = (resp.retorno==2) ? oRows.ganadorJuego((resp.ganadorPartida+" ganó!")) : oRows.ganadorJuego((resp.ganador+" ganó!"));
                                       var gw = oRows.ganadorJuego(("Con "+resp.Porcentaje+" "+resp.ganadorPartida+" ganó el juego!"));
                                        gw.open();
                                       },900);   
                                }else if(resp.retorno==1 && resp.ganador){
                                  setTimeout(function(){
                                        //var gw = (resp.retorno==2) ? oRows.ganadorJuego((resp.ganadorPartida+" ganó!")) : oRows.ganadorJuego((resp.ganador+" ganó!"));
                                       var gw = oRows.ganadorJuego((resp.ganador+" ganó la partida!"));
                                        gw.open();
                                       },900);   
                                }  
                                 
                                    
                               }else{
                                   alert("Error al enviar el dibujo.");
                               }
                           }
                       });
                       
                    }
					aEnviar.addEventListener('click',aEnviarListener);
				
					
					// Add to the parent view.
					winJuegoII.add(aLabelCr);
					winJuegoII.add(aLabelJ);
					winJuegoII.add(wv);
					winJuegoII.add(anImageViewJ);
					winJuegoII.add(aEnviar);
					
		
		// %&&%&%&%&%&%&%&%&%&&%&%&%&%&%&%&%&%&&%&%&%&%&%&%&%&%&&%&%&%&&%&%& //

   		// ********************************VENTANA SELECCIÓN DE COLOR **************************************** //
		
		var banedColor="";
		
		/// funcion para preparar la vista cuando se va a copiar dibujo //
		
		function copyImageMode(){
		    Ti.API.info("Modo copiar: "+imagenPCopiar);
		    aLabel.setLeft(5);
		   var casheBuster = new Date();
            imagenCopyOriginalImg.setImage((imagenPCopiar+'?casheBuster='+casheBuster.getTime()));
            imagenCopyMuestraImg.updateLayout({image:(imagenPCopiar+'?casheBuster='+casheBuster.getTime())});
            // setTimeout(function(){
                // Ti.API.info("copyimageTimeout ");
               // imagenCopyMuestraImg.updateLayout({image:imagenPCopiar});
            // imagenCopyOriginalImg.setImage(imagenPCopiar); 
            // },5000);
		    
		    imagenCopyMuestraImg.show();
		    segundosRestantes=10;
		    aLabelCr.setText(''+segundosRestantes+' Seg');
		}
		function newImageMode(){
            Ti.API.info("Modo noCopiar: "+imagenPCopiar);
            aLabel.setLeft(100);
            imagenCopyMuestraImg.hide();
            segundosRestantes=15;
        }
		
		var winJuegoI = Titanium.UI.createWindow({  
		    		title:'Color',
				    backgroundImage:'Images/JuegoIa.png',
				    barColor: 'none'
					});
					
					
					
		var aJugarIphone = {
                        backgroundImage:'Images/BtnInvitar.png',
                        title : '  Iniciar Juego',
                        font:{fontSize:16,fontFamily:'Myriad Pro',color:'#006666'},
                        textAlign:'center',
                        height : 36,
                        width : 210,
                        bottom : 15
                };
                var aJugarIpad = {
                        backgroundImage:'Images/BtnInvitar.png',
                        title : '  Iniciar Juego',
                        font:{fontSize:36,fontFamily:'Myriad Pro',color:'#006666', fontWeight:'bold'},
                        textAlign:'center',
                        height : 66,
                        width : 310,
                        bottom : 15
                };
					// Create a Button.
					var aJugar = Ti.UI.createButton((platform ? aJugarIpad : aJugarIphone));
		
		var imagenCopyMuestraImg = Ti.UI.createImageView({
            top:5,
            right:5,
            width:70,
            height:70,
            image:imagenPCopiar
        });
        
        imagenCopyMuestraImg.addEventListener('click', function(){
            imagenCopyOriginalImg.show();
        });
        
        var imagenCopyOriginalImg = Ti.UI.createImageView({
           // top:5,
           // right:5,
            width:288,
            height:288,
            image:imagenPCopiar
        });
        imagenCopyOriginalImg.addEventListener('click', function(){
            imagenCopyOriginalImg.hide();
        });
        
        imagenCopyOriginalImg.hide();
				imagenCopyMuestraImg.hide();	
					//VIEWS
				    
				    var viewColor = Ti.UI.createView({
				        top: (platform ? 126 : 49),
				        left: (platform ? 110 : 45),
				        width: (platform ? 155 : 68),
				        height: (platform ? 54 : 25),
				        backgroundColor:'#000000'
				    });
				    
				    var viewOrange = Ti.UI.createView({
				        top: (platform ? 330 : 130),
                        left: (platform ? 210 : 85),
                        action: '#ff3300',
                        width:  (platform ? 85 : 40),
                        height:  (platform ? 85 : 40),
				    });
				    
				    var viewYellow = Ti.UI.createView({
				        bottom: (platform ? 365 : 140),
                        left: (platform ? 160 : 70),
                        action: '#ffff00',
                        width: (platform ? 95 : 40),
                        height: (platform ? 85 : 40),
				    });
				    
				    var viewPink = Ti.UI.createView({
				        bottom: (platform ? 250 : 96),
                        left: (platform ? 280 : 119),
                        action: '#ff0066',
                        width: (platform ? 95 : 40),
                        height: (platform ? 95 : 40),
				    });
				    var viewBlue = Ti.UI.createView({
				        top: (platform ? 176 : 66),
                        left: (platform ? 365 : 150),
                        action: '#3399ff',
                        width: (platform ? 120 : 40),
                        height: (platform ? 120 : 40),
				    });
				    
				    var viewPurple = Ti.UI.createView({
				        top: (platform ? 265 : 109),
                        right: (platform ? 150 : 60),
                        action: '#990066',
                        width: (platform ? 95 : 45),
                        height: (platform ? 95 : 40),
				    });
				    
				    var viewGreen = Ti.UI.createView({
				        top: (platform ? 425 : 170),
                        right: (platform ? 145 : 58),
                        action: '#33cc33',
                        width: (platform ? 95 : 50),
                        height: (platform ? 95 : 40),
				    });
				    
				    var viewViolet = Ti.UI.createView({
				        bottom: (platform ? 280 : 106),
                        right: (platform ? 230 : 100),
                        action: '#663399',
                        width: (platform ? 95 : 40),
                        height: (platform ? 95 : 40),
				    });
				    
				    var viewBlack = Ti.UI.createView({
				        top: (platform ? 425 : 160),
                        right: (platform ? 340 : 128),
                        action: '#000000',
                        width: (platform ? 95 : 55),
                        height: (platform ? 95 : 55),
				    });
					//END VIEWS
					
					//Listener for screen click
					winJuegoI.addEventListener('click', function(e){
					    if(e.source.action){
					        Ti.API.info('banedColor '+banedColor+' e.source.action: '+e.source.action);
					        if(banedColor!=e.source.action){
					            viewColor.setBackgroundColor(e.source.action);
					        }else{
					            alert("No puedes escoger el mismo color.");
					        }
					    }
					});
					
					aJugar.addEventListener('click', function() {
					    if(banedColor!=(viewColor.getBackgroundColor())){
                                 winJuegoII.open({modal:true});
                            }else{
                                alert("No puedes escoger el mismo color.");
                            }
					   
					});
					
					// Create a Label.
					var aLabel = Ti.UI.createLabel({
						text : 'Seleccione un Color',
						color : '#fff',
						font : {fontSize:18, fontFamily:'Myriad Pro', fontWeight:'normal'},
						top : 13,		
						textAlign : 'left'		
					});
				
			winJuegoI.add(aLabel);	
			winJuegoI.add(aJugar);
			winJuegoI.add(viewColor);	
			winJuegoI.add(viewOrange);	
			winJuegoI.add(viewYellow);
			winJuegoI.add(viewPink);
			winJuegoI.add(imagenCopyMuestraImg);
			winJuegoI.add(viewBlue);
			winJuegoI.add(viewViolet);
			winJuegoI.add(viewGreen);
			winJuegoI.add(viewPurple);
			winJuegoI.add(viewBlack);
			winJuegoI.add(imagenCopyOriginalImg);
					
		// *************************************************************************************************** //	
		////      Ventana de juegos   en espera    ////
    var oRows = require('juegos/tools');
    var alertJuegoEnviado = require('juegos/alertJuegoEnviado');
    var alertJuegoPorEnviar = require('juegos/alertJuegoPorEnviar');
    
    var JueRefreshButton = Ti.UI.createButton({
       top:0,
       left:0,
       width:120,
       systemButton:Titanium.UI.iPhone.SystemButton.REFRESH,
       height:38,
       });
    
        JueRefreshButton.addEventListener('click',function(){
            oRows.tableJuegos(idpp,(""+config.sitio+"Juegos_Espera.aspx"),table1, idpp);
        });
    
	var winJuegos = Titanium.UI.createWindow({  
	    title:'Juegos',
	    rightNavButton: JueRefreshButton
	});
	
	var tab1 = Titanium.UI.createTab({ 
	    icon:'Lapiz.png',
	    title:'Juegos',
	    backgroundImage:'/FondoDefault.png',
	    window:winJuegos
	});
	  //Create searchBar
	  var search = Titanium.UI.createSearchBar({
	     barColor:'#000', 
	     showCancel:true,
	     height:43,
	     top:0
	  });
    
	  // define the tableview and assign its data/rows here
	  var table1 = Ti.UI.createTableView({
	   width:'90%',
	   height:'80%',
	   top:10,
	   backgroundColor:'transparent',
	   separatorStyle:(Ti.Platform.osname=='iphone') ? Titanium.UI.iPhone.TableViewSeparatorStyle.NONE : null,
	   search:search
	  });
	  
	  table1.addEventListener('click', function(e) {
	      Ti.API.info(JSON.stringify(e.row));
	  	if(e.row.status == '0')
	  	{
	  	    var ca = alertJuegoEnviado({
	  	        fecha: e.row.fecha,
	  	        hora: e.row.hora,
	  	        nuevo: e.row.nuevo,
                hide:function(){
                    winJuegos.remove(ca);
                }  
	  	    });
	  	    winJuegos.add(ca);
	  	}
	  	else
	  	{
	  	    idJuego=e.row.idpp;
	  	    if(e.row.nuevo == 0){
                   banedColor=e.row.colorBan;
                   imagenPCopiar=e.row.imagenCopiar;	  	     
                   copyImageMode();   
	  	    }else{
	  	        banedColor="";
	  	        newImageMode();
	  	    }
	  	    var ca2 = alertJuegoPorEnviar({
	  	        title: 'Continuar Jugando',
	  	        content: (e.row.nuevo == 0 ? e.row.nombre + " ha enviado su dibujo." : "        Comenzar Partida."),
	  	        image: e.row.imagen,
	  	        initGame:function(){
	  	           winJuegos.remove(ca2); 
	  	           tabGroup.activeTab.open(winJuegoI,{animated:true});
	  	        },
	  	        hide:function(){
                    winJuegos.remove(ca2);
                } 
	  	    });
	  	    winJuegos.add(ca2);
	  	}
	  });
  
	
	var aMulti = Ti.UI.createButton({
		title : '      Jugar en tu Dispositivo',
		backgroundImage:'Images/Multi.png',
		font:{fontSize:16,fontFamily:'Myriad Pro'},
		color:'#ffffff',
		textAlign:'center',
		height : 36,
		width : 210,
		bottom: 15
	});
	
	// Listen for click events.
	aMulti.addEventListener('click', function() {
	    
	    tools.httprequest({
                           url:(""+config.sitio+"Juegos_Espera.aspx"),
                           data:{
                               Num_Tipo:2,
                               ID__Jugador1:idpp,
                              ID__Jugador2 : idpp,
                           },
                           callback:function(resp){
                              resp=JSON.parse(resp);
                              if(resp.retorno==1){
                                  banedColor="";
                                    newImageMode();
                                  idJuego=resp.idJuego;
                              tabGroup.activeTab.open(winJuegoI,{animated:true});
                              }
                           }
                       });
	    
	    
		//tabGroup.activeTab.open(winJuegoI,{animated:true});
	});
	
	winJuegos.add(aMulti);
	winJuegos.add(table1);
	
	
//
// ****************************************************VENTANA AMIGOS*************************************************************
//
    var amigosTools = require('amigos/tools');
    var alertSolJuego = require('amigos/alertSolicitudJuego');
    
    var amigRefreshButton = Ti.UI.createButton({
    top:0,
    left:0,
    width:120,
    systemButton:Titanium.UI.iPhone.SystemButton.REFRESH,
    height:38,
    });
    
    amigRefreshButton.addEventListener('click',function(){
        amigosTools.tableAmigos(idpp,(""+config.sitio+"Amigos.aspx"),tableAmigos);
    });
    
	var winAmigos = Titanium.UI.createWindow({  
	    title:'Amigos',
	    rightNavButton:amigRefreshButton
	});
	
	var tab2 = Titanium.UI.createTab({  
	    icon:'Amigos2.png',
	    title:'Amigos',
	    backgroundImage:'/FondoDefault.png',
	    window:winAmigos
	});

	  //Create searchBar
	  var search = Titanium.UI.createSearchBar({
	     barColor:'#000', 
	     showCancel:true,
	     height:43,
	     top:0
	  });
  
	  // define the tableview and assign its data/rows here
	  var tableAmigos = Ti.UI.createTableView({
	   width:'90%',
	   height:'80%',
	   top:10,
	   backgroundColor:'transparent',
	   separatorStyle:(Ti.Platform.osname=='iphone') ? Titanium.UI.iPhone.TableViewSeparatorStyle.NONE : null,
	   search:search
	  });
	  
	  tableAmigos.addEventListener('click', function(e) {
          Ti.API.info(JSON.stringify(e));
          
             
            var ca = alertSolJuego({
             ppid:idpp,
            url:(""+config.sitio+"Invitaciones_Juegos.aspx"),
            idUserAmigo:e.row.idpp,
            content:'¿Enviar invitación para jugar?',
            hide:function(){
                winAmigos.remove(ca);
            }
         });
         winAmigos.add(ca);  
              
      });
	  
	  
	 

	// add table view to the window
	winAmigos.add(tableAmigos);

// ****************************************************VENTANA NOTIFICACIONES**************************************************************
    
    var notifTools = require('notificaciones/tools');
    var alertSolAmiObj = require ('customAlertSolicitudAmigo');
    var alertNotifObj = require ('notificaciones/alertNotif');
    
    var notifRefreshButton = Ti.UI.createButton({
    top:0,
    left:0,
    width:120,
    systemButton:Titanium.UI.iPhone.SystemButton.REFRESH,
    height:38,
    });
    
    notifRefreshButton.addEventListener('click',function(){
        notifTools.getInv(idpp,(""+config.sitio+"Notificaciones.aspx"),tableN);
    });
    
	var winNoti = Titanium.UI.createWindow({  
	    title:'Notificaciones',
	    rightNavButton:notifRefreshButton
	});
	
	var tab3 = Titanium.UI.createTab({  
	    icon:'Notificacion.png',
	    title:'Notificaciones',
	    backgroundImage:'/FondoDefault.png',
	    window:winNoti
	});
	
	  // define the tableview and assign its data/rows here
	  var tableN = Ti.UI.createTableView({
	   width:'90%',
	   height:'80%',
	   top:10,
	   backgroundColor:'transparent',
	   separatorStyle:(Ti.Platform.osname=='iphone') ? Titanium.UI.iPhone.TableViewSeparatorStyle.NONE : null,
	  });
	  	 
	  tableN.addEventListener('click', function(e) {
	      Ti.API.info(JSON.stringify(e));
	      
	      if(e.row.title=="Invitación Amistad"){
	          
         var alertN = new alertNotifObj({
             callback:function(eee){
                 var res = JSON.parse(eee);
                 alert(res.mensaje);
                 tableN.deleteRow(e.index);
                 winNoti.remove(alertN);
             },
             title:e.row.title,
             rechazada:false,
             dataAceptar:{'ID__Usuario':idpp ,'ID__UsuarioAmigo':e.row.idUserAmigo , 'Num_Tipo':3, 'ID__Invitacion':e.row.idInv},
             dataRechazar:{'ID__Usuario':idpp ,'ID__UsuarioAmigo':e.row.idUserAmigo , 'Num_Tipo':4, 'ID__Invitacion':e.row.idInv},
             url: (""+config.sitio+"Invitaciones_Amigos.aspx"),
             content:(""+e.row.user+" quiere ser tu amigo.")
         });
         winNoti.add(alertN);    
         
	      }else if(e.row.title=="Invitación Juego"){
	          
	          var alertN = new alertNotifObj({
             callback:function(eee){
                 var res = JSON.parse(eee);
                 alert(res.mensaje);
                 tableN.deleteRow(e.index);
                 winNoti.remove(alertN);
             },
             title:e.row.title,
             rechazada:false,
             dataAceptar:{'ID__Usuario':idpp ,'ID__UsuarioAmigo':e.row.idUserAmigo , 'Num_Tipo':3},
             dataRechazar:{'ID__Usuario':idpp ,'ID__UsuarioAmigo':e.row.idUserAmigo , 'Num_Tipo':4},
             url: (""+config.sitio+"Invitaciones_Juegos.aspx"),
             content:(""+e.row.user+" te invitó a jugar.")
         });
         winNoti.add(alertN);  
	          
	      }else{
	          var alertN = new alertNotifObj({
             callback:function(eee){
                 tableN.deleteRow(e.index);
                 winNoti.remove(alertN);
             },
             title:e.row.title,
             rechazada:true,
             dataAceptar:{'ID__Usuario':idpp ,'ID__UsuarioAmigo':e.row.idUserAmigo , 'Num_Tipo':2, 'ID__Invitacion':e.row.idInv},
             dataRechazar:{'ID__Usuario':idpp ,'ID__UsuarioAmigo':e.row.idUserAmigo , 'Num_Tipo':2, 'ID__Invitacion':e.row.idInv},
             url: (""+config.sitio+"Notificaciones.aspx"),
             content:(""+e.row.user+" ha rechazado tu solicitud de amistad.")
         });
         winNoti.add(alertN);   
	          
	      }
	      
	      
	  });

  	winNoti.add(tableN);
  	

//	
// **************************************************** VENTANA CONFIGURACIÓN **************************************************************
//
	
	//********************* Ventana Consecuente de Notificaciones Push ************************ //
	
		var winPush = Titanium.UI.createWindow({  
	    title:'Notificaciones Push',
	    backgroundImage:'/FondoDefault.png'
		});
		
		// Create a Label.
		var labelP = Ti.UI.createLabel({
			text : '¿Deseas Recibir Notificaciones Push?',
			color: '#003333',
			font : {fontSize:16, fontFamily:'Myriad Pro', fontWeight:'normal'},
			top : 24,		
			textAlign : 'center'		
			});
		
		// Create a Switch.
		var SwitchP = Ti.UI.createSwitch({
			value : true,
			top : 59
		});
		
		// Create an ImageView.
		var anImageView = Ti.UI.createImageView({
			backgroundImage:'Images/FondoII.png',
			backgroundColor:'transparent',
			selectedColor : 'transparent',
			height:100,
			width:300,
			top:8
		});
		
		anImageView.addEventListener('load', function() {
			Ti.API.info('Image loaded!');
		});
		
		
		// Listen for change events.
		SwitchP.addEventListener('change', function(e) {
			Ti.API.info('Event value: ' + e.value + ', switch value: ' + aSwitch.value);
		});
		
		// Add to the parent view.
		winPush.add(anImageView);
		winPush.add(labelP);
		winPush.add(SwitchP);
		
	
	//********************* Ventana Consecuente de Modificar Perfil ************************ //
	
			// &%&&%&%&%&%&%&%&%& Subventana Modificar Perfil &%&&%&%&%&%&%&%&%& //
			
					var winUserNameSub = Titanium.UI.createWindow({  
		    		title:'Perfil',
				    backgroundImage:'/FondoDefault.png'
					});
					
					winUserNameSub.addEventListener('close',function(){
					    Ti.API.info('mod perfil sub close '+imagenpp);
					    ImgPerfil.setImage(imagenpp);
					});

					// Create an ImageView.
					var configImgPerfil = Ti.UI.createImageView({
						image : imagenpp,
						width : 80,
						height : 80,
						top : 30
					});
					
					configImgPerfil.addEventListener('click', function(e) {
						tools.getImage(function(nvaImagen){
                        tools.httprequest({
                            data:{
                                'Bnd_Tipo':1,
                                'Vch_Foto':nvaImagen,
                                'ID__Usuario':idpp
                            },
                            url: (""+config.sitio+"Modificar_UserName_Foto.aspx"),
                            callback:function(e){
                                 e=JSON.parse(e);
                               if(e.retorno==1){
                                   alert(e.mensaje);
                                  configImgPerfil.setImage(nvaImagen);
                                  imagenpp=e.imagen;
                                  
                                  Ti.App.Properties.setString('imagen', imagenpp);
                               }else{
                                   alert(e.mensaje);
                               }
                                }
                        });
                    });
					});
					
					
					// Create a Label.
					var labelSub = Ti.UI.createLabel({
						text : 'Ingresa tu Nuevo UserName: ',
						color : '#FFF',
						font : {fontSize:16, fontFamily:'Myriad Pro', fontWeight:'normal'},
						top : 120,		
						textAlign : 'center'
					});
					
					var txtUser = Ti.UI.createTextField({
					    borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
					    color: '#003333',
					    top: 150,
					    autocapitalization:false,
					    autocorrect :false,
					    width: 230, 
					    height: 30
					});
					
					
					// Botón de Validar
					var ValidarSub = Ti.UI.createButton({
						title : '  Validar',
						backgroundImage:'images/BtnValidar.png',
						font:{fontSize:17,fontFamily:'Myriad Pro'},
						textAlign:'center',
						height : 36,
						width : 210,
						top : 190
						});
				
					// Listen for click events.
					ValidarSub.addEventListener('click', function() {
						tools.httprequest({
                            data:{
                                'Bnd_Tipo':0,
                                'ID__Usuario':idpp,
                                'Vch_UserName':(txtUser.getValue())
                            },
                            url: (""+config.sitio+"Modificar_UserName_Foto.aspx"),
                            callback:function(response){
                              response = JSON.parse(response);
                              alert(response.mensaje);
                             if(response.retorno==1){
                                 
                                 usernamepp=txtUser.getValue();
                                 labelU2.setText(usernamepp);
                                 txtUser.setValue('');
                                 Ti.App.Properties.getString(usernamepp);
                                 }
                             Ti.API.info("new username: "+usernamepp);
                                }
                        });
					});
					

					winUserNameSub.add(configImgPerfil);
					winUserNameSub.add(labelSub);
					winUserNameSub.add(txtUser);
					winUserNameSub.add(ValidarSub);
					
				// &%&&%&%&%&%&%&%&%&           Configurar Perfil                 &%&&%&%&%&%&%&%&%& //
	
		var winUserName = Titanium.UI.createWindow({  
	    title:"Perfil",
	    backgroundImage:'/FondoDefault.png'
		});
		
		// Create an ImageView.
					var ImgPerfil = Ti.UI.createImageView({
						image : imagenpp,
						width : 57,
						height : 57,
						top : 30
					});
					
						
		
		// Create a Label.
		var labelU1 = Ti.UI.createLabel({
			text : 'Username: ',
			color : '#000',
			font : {fontSize:16, fontFamily:'Myriad Pro', fontWeight:'normal'},
			top : 100
		});
		
		// Create a Label.
		var labelU2 = Ti.UI.createLabel({
			text : usernamepp,
			color : '#000',
			font : {fontSize:16, fontFamily:'Myriad Pro', fontWeight:'normal'},
			top : 120
		});
		
		// Botón de Modificar
		var ModificarU = Ti.UI.createButton({
			title : '       Modificar',
			backgroundImage:'images/BtnModificar.png',
			font:{fontSize:17,fontFamily:'Myriad Pro'},
			textAlign:'center',
			height : 36,
			width : 210,
			top : 155
			});
	
		// Listen for click events.
		ModificarU.addEventListener('click', function() {
			tabGroup.activeTab.open(winUserNameSub,{animated:true});
		});
		
		
		// Add to the parent view.
		winUserName.add(ModificarU);
		winUserName.add(ImgPerfil);
		winUserName.add(labelU1);
		winUserName.add(labelU2);
	
	//************* Ventana Consecuente de Amigos ************************

		// &%&&%&%&%&%&%&%&%& Subventana Agregaar Contactos &%&&%&%&%&%&%&%&%& //
		
		var agrConTools = require('configuracion/porUsername');
				var winContactos = Titanium.UI.createWindow({  
		    		title:'Agregar Contactos', 
				   backgroundImage:'/FondoDefault.png'
					});
					
			 var usernametf = Ti.UI.createTextField({
                    top:20,
                    width:220,
                    height:38,
                    borderStyle:Ti.UI.INPUT_BORDERSTYLE_BEZEL,
                    backgroundColor:'white',
                    hintText:'Busca un usuario',
                    autocapitalization:false,
                    autocorrect:false,
                }); 
		
			// Create a Button.
				var buscarContactosButton = Ti.UI.createButton({
					backgroundImage:'Images/SendInv.png',
					title : '       Buscar contacto',
					font:{fontSize:16,fontFamily:'Myriad Pro',color:'#006666'},
					textAlign:'center',
					height : 36,
					width : 200,
					top: 55
				});
				
				var tablaAgregarContactos = Ti.UI.createTableView({
				    height:400,
				    top:101,
				    backgroundColor:'transparent',
				    width:300
				});
				
				tablaAgregarContactos.addEventListener('click',function(e){
				    Ti.API.info(e);
				    agrConTools.filaSeleccionada({
				        callback:function(){
				            tools.httprequest({
                            data:{
                                'Num_Tipo':2,
                                'ID__Usuario':idpp,
                                 'ID__UsuarioAmigo':e.row.idUser
                            },
                            url: (""+config.sitio+"Invitaciones_Amigos.aspx"),
                            callback:function(e){
                                 var res=JSON.parse(e);
                                 alert(res.mensaje)
                                }
                        });
				                },
				        
				        
				    });
				});
				
				winContactos.add(tablaAgregarContactos);
			// Listen for click events.
				buscarContactosButton.addEventListener('click', function() {
					usernametf.blur();
					
					if(winContactos.tipo=='correo'){
					    var dataWs = {
                                'Num_Tipo':3,
                               'Vch_Correo':(winContactos.tipo=='correo'? (usernametf.getValue()) : ''),
                                'ID__Usuario':idpp
                        };
					}else{
					  var dataWs = {
                                'Num_Tipo':3,
                                'Vch_UserName':(winContactos.tipo=='correo'? '' : (usernametf.getValue())),
                                'ID__Usuario':idpp
                        };  
					}
					
					tools.httprequest({
                            data:dataWs,
                            url: (""+config.sitio+"Amigos.aspx"),
                            callback:function(e){
                                 
                                 e=JSON.parse(e);
                                 if (e.retorno==0){
                                     alert(e.mensaje);
                                 }else{
                                     var dataarray = [];
                                 for(var i=0;i<e.Amigos.length;i++){
                                     Ti.API.info(e.Amigos[i].Vch_UserName);
                                     var rowAC = new CustomRow({
                                     title:e.Amigos[i].Vch_UserName,
                                     image:e.Amigos[i].Vch_Foto
                                 });
                                 rowAC.idUser = e.Amigos[i].ID__Usuario;
                                 dataarray.push(rowAC);
                                 }
                                 tablaAgregarContactos.setData(dataarray);
                                 }
                                 
                                 
                                }
                        });
					
					
				});
				  
				  winContactos.add(buscarContactosButton);
				  winContactos.add(usernametf);
				  
				  winContactos.addEventListener('close',function(){
				      usernametf.setValue('');
				      tablaAgregarContactos.setData([]);
				  });
	  
	  		// &%&&%&%&%&%&%&%&%&    Ventana de agregar amigos   &%&&%&%&%&%&%&%&%& //
		
		var agregarPorContactos = require('configuracion/porContactos');
		
		var winAmigosA = Titanium.UI.createWindow({
	    title:'Amigos',
	    backgroundImage:'/FondoDefault.png'
		});
		
		// Create a Label.
		var labelA = Ti.UI.createLabel({
			text : 'Encuentra a tus Amigos por: ',
			color : '#FFF',
			fcolor : '#fff',
			font : {fontSize:16, fontFamily:'Myriad Pro', fontWeight:'normal'},
			top : 43,
			textAlign : 'center'
		});
		
		// Botón de Contactos
		var ContactosA = Ti.UI.createButton({
			title : '          Contactos',
			backgroundImage:'Images/BtnContact.png',
			font:{fontSize:16,fontFamily:'Myriad Pro'},
			textAlign:'left',
			height : 36,
			width : 220,
			top : 100
			});
	
			// Listen for click events.
			ContactosA.addEventListener('click', function() {
			    winContactos.tipo='contactos';
			    var emailsContactos = [];
                var addressBookDisallowed = function(){
                alert('No hay autorización para leer sus contactos.');
                };
                if (Ti.Contacts.contactsAuthorization == Ti.Contacts.AUTHORIZATION_AUTHORIZED){
                    emailsContactos = agregarPorContactos.performAddressBookFunction();
                } else if (Ti.Contacts.contactsAuthorization == Ti.Contacts.AUTHORIZATION_UNKNOWN){
                    Ti.Contacts.requestAuthorization(function(e){
                        if (e.success) {
                            emailsContactos = agregarPorContactos.performAddressBookFunction();
                        } else {
                            addressBookDisallowed();
                        }
                    });
                } else {
                    addressBookDisallowed();
                }
                var winCon = agregarPorContactos.ventanaAgregarContactos({
                    emails:emailsContactos,
                    idpp:idpp,
                    sitio:config.sitio
                });
				tabGroup.activeTab.open(winCon,{animated:true});
			});
			
		// Botón de Correo Electrónico
		var CorreoA = Ti.UI.createButton({
			title : '          Correo Electrónico',
			backgroundImage:'Images/BtnEmail.png',
			font:{fontSize:16,fontFamily:'Myriad Pro'},
			textAlign:'left',
			height : 36,
			width : 220,
			top : 165
			});
	
			// Listen for click events.
			CorreoA.addEventListener('click', function() {
			    winContactos.tipo='correo';
			    
				tabGroup.activeTab.open(winContactos,{animated:true});
			});
		
		// Botón de UserName
		var UserA = Ti.UI.createButton({
			title : '          UserName',
			backgroundImage:'Images/BtnUserName.png',
			font:{fontSize:16,fontFamily:'Myriad Pro'},
			textAlign:'left',
			height : 36,
			width : 220,
			top : 230,
			});
	
			// Listen for click events.
			UserA.addEventListener('click', function() {
			    winContactos.tipo='username';
				tabGroup.activeTab.open(winContactos,{animated:true});
			});
			
			winAmigosA.add(labelA);
			winAmigosA.add(ContactosA);
			winAmigosA.add(CorreoA);
			winAmigosA.add(UserA);

//...............................................................................................................................//
	
	var winConfi = Titanium.UI.createWindow({  
	    title:'Configuración',
	    backgroundImage:'/FondoDefault.png',
	});
	
	var tab4 = Titanium.UI.createTab({  
	    icon:'Configuracion.png',
	    title:'Configuración',
	    window:winConfi
	});
	 
	// Botón de Notificaciones Push.
	var aNoti = Ti.UI.createButton({
		title : ' Notificaciones Push',
		backgroundImage:'Images/BtnPush.png',
		font:{fontSize:17,fontFamily:'Myriad Pro'},
		textAlign:'left',
		height : 36,
		width : 220,
		top : 80
	});
	
	// Listen for click events.
	aNoti.addEventListener('click', function() {
		tabGroup.activeTab.open(winPush,{animated:true});
	});

	// Botón de Modificar Perfil
	var aPerfil = Ti.UI.createButton({
		title : ' Modificar Perfil',
		backgroundImage:'Images/BtnPerfil.png',
		font:{fontSize:17,fontFamily:'Myriad Pro'},
		textAlign:'left',
		height : 36,
		width : 220,
		top : 140
	});
	
	// Listen for click events.
	aPerfil.addEventListener('click', function() {
		tabGroup.activeTab.open(winUserName,{animated:true});
	});
	
	// Botón de Amigos
	var aAmigos = Ti.UI.createButton({
		title : ' Amigos',
		backgroundImage:'Images/BtnAmigos.png',
		font:{fontSize:17,fontFamily:'Myriad Pro'},
		textAlign:'left',
		height : 36,
		width : 220,
		top : 200
	});
	
	// Listen for click events.
	aAmigos.addEventListener('click', function() {
		tabGroup.activeTab.open(winAmigosA,{animated:true});
	});
	
	// Botón de Salir
	var aSalir = Ti.UI.createButton({
		title : ' Salir',
		backgroundImage:'Images/BtnSalir.png',
		font:{fontSize:17,fontFamily:'Myriad Pro'},
		textAlign:'left',
		height : 36,
		width : 220,
		top : 260
	});

	// Listen for click events.
	aSalir.addEventListener('click', function() {
	    Ti.API.info('Salir');
		tabGroup.close();
		Ti.App.Properties.removeProperty('id');
        Ti.App.Properties.removeProperty('imagen');
        Ti.App.Properties.removeProperty('username');
		var loginW = new loginObj(initGame);
        loginW.open();
	});

	winConfi.add(aAmigos);
	winConfi.add(aSalir);
	winConfi.add(aPerfil);
	winConfi.add(aNoti);
		
//
// **************************************************** VENTANA HISTORIAL **************************************************************
//
    
    var histTools = require('historial/tools');
    
    var histRefreshButton = Ti.UI.createButton({
    top:0,
    left:0,
    width:120,
    systemButton:Titanium.UI.iPhone.SystemButton.REFRESH,
    height:38,
    });
    
    histRefreshButton.addEventListener('click',function(){
        tbldata = []
        tableHistorial.setData(tbldata); 
        
        histTools.getHistorial({
        url:(""+config.sitio+"Juegos_Ganados.aspx"),
        data:{
            ID__Usuario:idpp,
            Num_Tipo:1
        },
        callback:fillHistorialTable
    });
    });
 
	var winHistorial = Titanium.UI.createWindow({  
	    title:'Historial',
	    backgroundImage:'/FondoDefault.png',
	    rightNavButton:histRefreshButton
	});
	
	var tabHistorial = Titanium.UI.createTab({  
	    icon:'Historial2.png',
	    title:'Historial',
	    window:winHistorial
	});
	
	var CustomRowHist = require('CustomRowHist');
	
	histTools.getHistorial({
	    url:(""+config.sitio+"Juegos_Ganados.aspx"),
	    data:{
	        ID__Usuario:idpp,
	        Num_Tipo:1
	    },
	    callback:fillHistorialTable
	});
	
	var tbldata = [];
      function fillHistorialTable(res){
          
          res = JSON.parse(res);
          for(var i=0;i<res.Historial.length;i++) {
         //     Ti.API.info(res.Historial[i]);
       tbldata.push(new CustomRowHist(res.Historial[i]));
      }
         tableHistorial.setData(tbldata); 
      }    
      
	  // declare an array to hold your table rows
	  
	  //var nombres=['Cinthia Barragán','Jorge Curiel','Daniela Alatorre','Perla Salinas','Carlos Martinez','Karla Ortiz','Sergio Virgen','Manuela Martinez'];
	  // use a loop to create at least 8 rows, instantiate a new custom row in each iteration
	  
  
	  //Create searchBar
	  var search = Titanium.UI.createSearchBar({
	     barColor:'#000', 
	     showCancel:true,
	     height:43,
	     top:0
	  });
  
  
  // define the tableview and assign its data/rows here
  var tableHistorial = Ti.UI.createTableView({
   width:'90%',
   height:'93%',
   top:10,
   backgroundColor:'transparent',
   separatorStyle:(Ti.Platform.osname=='iphone') ? Titanium.UI.iPhone.TableViewSeparatorStyle.NONE : null,
   search:search
  });
  
  
  winHistorial.add(tableHistorial);
	
	
//
//  add tabs
//
tabGroup.addTab(tab1);  
tabGroup.addTab(tab2);  
tabGroup.addTab(tab3);  
tabGroup.addTab(tab4); 
tabGroup.addTab(tabHistorial);  

        

// open tab group
tabGroup.open({transition: Titanium.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT});

}