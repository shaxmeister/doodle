var CustomRow = function(/*Object*/ _params) {
    
    var titleRow = "";
    switch(_params.Num_Tipo){
        case '1':
        titleRow='Invitación Amistad'
        break;
        case '2':
        titleRow='Invitación Juego'
        break;
        case '3':
        titleRow='Invitación Rechazada'
        break;
    }
	var row = Ti.UI.createTableViewRow({
		backgroundImage:'Images/off_1.png',
		selectedBackgroundImage:'Images/on_1.png',
		backgroundColor:'transparent',
		color:'navy',
		selectedColor : 'white',
		height:50,
		user:_params.Vch_UserName,
		idInv:_params.ID__Invitacion,
		idUserAmigo:_params.ID__UsuarioAmigo,
		title:titleRow,
	});
	
    

	return row;
};

module.exports = CustomRow;