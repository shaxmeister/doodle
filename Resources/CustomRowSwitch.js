var CustomRow = function(/*Object*/ _params) {
	var row = Ti.UI.createTableViewRow({
		backgroundImage:'Images/off_4.png',
		selectedBackgroundImage:'Images/on_4.png',
		backgroundColor:'transparent',
		color:'transparent',
		selectedColor : 'transparent',
		height:50,
		title:_params.primarylabel,
		className:'tablaJJ'
	});
	
	var rowImage = Ti.UI.createImageView({
		image:'5.png',
		height:34,
		width:64,
		top:8,
		left:0,
		myImage:_params.myImage //Variable personalizada
	});
	
	row.add(rowImage);

	var primaryLabel = Ti.UI.createLabel({
		text:_params.primarylabel,
		font:{
			fontSize:16,
			fontWeight:'normal'
		},
		color:'#003333',
		left:60
	});
	
	row.add(primaryLabel);
	var basicSwitch = Ti.UI.createSwitch({
  	value:true, // mandatory property for iOS 
  	right: 9,
  	height:10,
  	width:20,
  	rownum : _params.rownum
	});
	basicSwitch.addEventListener('change',function(e){
			Ti.API.info(JSON.stringify(e));
	});
	row.add(basicSwitch);

	return row;
};

module.exports = CustomRow;