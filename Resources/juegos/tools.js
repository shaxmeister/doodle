function juegosRow(params,idUsuario)
{
    var row = Ti.UI.createTableViewRow({
        className: 'row',
        height: 50,
        backgroundImage:'Images/off_2.png',
        selectedBackgroundImage:'Images/on_2.png',
        backgroundColor:'transparent',
        color:'navy',
        selectedColor : 'white',
        idpp:params.ID__Juego,
        fecha: params.Fec_Fecha,
        hora: params.Fec_Fecha,
        imagen: params.Vch_Foto,
        nombre: params.Vch_UserName,
        imagenCopiar: params.Vch_Imagen,
        colorBan:params.Vch_Color,
        nuevo: params.Bnd_New,
        status: (idUsuario == params.ID__Jugador1 ? ((params.Num_Turno == 1 || params.Num_Turno == 2) ? '1' : '0') : ((params.Num_Turno == 1 || params.Num_Turno == 2) ? '0' : '1')),
        layout:'horizontal'
    });
    if(params.ID__Jugador1==params.ID__Amigo){
    row.status=1;
    var status1 = Ti.UI.createImageView({
        image:'Images/Disponible.png',
        left: 10,
        height:5,
        width:5
    });
    row.add(status1);   
    }else{
        
        var status1 = Ti.UI.createImageView({
        image:(params.Num_Turno == 1 || params.Num_Turno == 2 ? 'Images/Disponible.png' : 'Images/NoDisponible.png'),
        left: 10,
        height:5,
        width:5
    });
    var status2 = Ti.UI.createImageView({
        image:(params.Num_Turno == 1 || params.Num_Turno == 2 ? 'Images/NoDisponible.png' : 'Images/Disponible.png'),      
        left: 10,
        height:5,
        width:5
    });
    if(idUsuario == params.ID__Jugador1)
    {
        row.add(status1);   
    }
    else
    {
        row.add(status2);
    } 
    }
   
    
    var imagen = Ti.UI.createImageView({
       image:params.Vch_Foto,
       width: 40,
       height: 40,
       top: 5 
    });
    row.add(imagen);
    
    var nombre = Ti.UI.createLabel({
        text: params.Vch_UserName,
        color: "black",
        width: 175
    });
    row.add(nombre);
    
    var vidas = Ti.UI.createLabel({
        text:params.Num_VidasJugador,
        color:"black"
    });
    row.add(vidas);
    
    return row;
};

exports.tableJuegos = function(id, url, tv, idUsuario){
    Ti.API.info("get Juegos");
    tv.setData([ ]);
 var client = Ti.Network.createHTTPClient({
      //Procesamos la info
     onload : function(e) {
         
         Ti.API.info("Juegos Received text: " + this.responseText);
         var res = JSON.parse(this.responseText);
         for(var i=0;i<res.JuegosenEspera.length;i++){
             var row = juegosRow(res.JuegosenEspera[i], idUsuario);
             tv.appendRow(row);
         }
     },
     onerror : function(e) {
         Ti.API.debug(JSON.stringify(e));
         //alert('error');
     },
     timeout : 5000 
 });
 client.open("POST", url);
 client.send({
     'Num_Tipo' : 1,
     'ID__Usuario':id
 }); 
    
    
};


///  Ventana con los resultados de un Round
exports.ganadorJuego = function(params){
  
  var t = Titanium.UI.create2DMatrix().scale(0);

// create a window with the initial transform scaled to 0
var w = Titanium.UI.createWindow({
  backgroundColor:'#336699',
  borderWidth:4,
  borderColor:'#999',
  height:300,
  width:300,
  borderRadius:10,
  opacity:0.92,
  transform:t
});

var textLbl = Ti.UI.createLabel({
    text:params.text,
    font:{fontSize:16 ,fontWeight:'regular'},
    height:Ti.UI.SIZE,
    width:Ti.UI.FILL,
    color:'black',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    bottom:8
    });
    w.add(textLbl);
    
    
 var ImgJug1 = Ti.UI.createImageView({
    top:0,
    left:0,
    width:200,
    height:200,
    image:''
});
var ImgJug2 = Ti.UI.createImageView({
    top:0,
    left:0,
    width:200,
    height:200,
    image:''
});

// create first transform to go beyond normal size
var t1 = Titanium.UI.create2DMatrix().scale(1.4);

var a = Titanium.UI.createAnimation();
a.transform = t1;
a.duration = 500;

w.addEventListener('open',function(){
    w.animate(a);
});

// when this animation completes, scale to normal size
a.addEventListener('complete', function()
{
    // we can use the identity transform to take it back to it's real size
  var t2 = Titanium.UI.create2DMatrix();
  w.animate({transform:t2, duration:200});
});

 w.addEventListener('click',closeW);
function closeW(){
    w.removeEventListener('click',closeW);
    w.close();
}
    Ti.API.info("ventana animada ganador "+text);
    return w;
};
