//Crear alert de juego por enviar
function customAlertJuegoPorEnviar(params)
{   
    // Create a Button.
    var okButton = Ti.UI.createButton({
        title : '   Jugar',
        backgroundImage:'Images/BtnAceptar.png',
        height : 36,
        color:'#fff',
        font:{
            fontSize:14,
            fontWeight:'normal'
        },
        width : 110,
        left : 14,
        bottom: 15
    });
    
    // Listen for click events.
    okButton.addEventListener('click', function() {
        params.initGame();
    });
    
    
    // Create a Button.
    var cancelButton = Ti.UI.createButton({
        backgroundImage:'Images/BtnRechazar.png',
        title : '     Cancelar',
        color:'#fff',
        font:{
            fontSize:14,
            fontWeight:'normal'
        },
        height : 36,
        width : 110,
        right : 14,
        bottom: 15
    });
    
    // Listen for click events.
    cancelButton.addEventListener('click', function() {
        params.hide();
    });
    
    
    // Create a Label.
    var titleLabel = Ti.UI.createLabel({
        text : params.title,
        color : 'black',
        font : {fontSize:18},
        width : Ti.UI.SIZE,
        top : 20,
        left : 65,
        textAlign : 'center'
    });
    
    // Create a Label.
    var texto = Ti.UI.createLabel({
        text : (''+params.content),
        color: '#000',
        font : {fontSize:15},
        width : Ti.UI.SIZE,
        top : 54,
        left : 33,
        textAlign : 'center'
    });
    
    
    // Create an ImageView.
    var iv = Ti.UI.createImageView({
        image : 'http://192.168.0.162:80/DoodleMatch/Imagenes/ImgDefault.png',
        width : 31,
        height : 36,
        top : 15,
        left : 23
    });
    


    
    var t = Titanium.UI.create2DMatrix().scale(0);
    var view = Titanium.UI.createView({
    backgroundImage:'Images/FondoAlert.png',
    borderWidth:3,
    borderColor:'#fff',
    height:150,
    width:260,
    borderRadius:20,
    opacity:0.92,
    transform:t
    });


// create first transform to go beyond normal size
var t1 = Titanium.UI.create2DMatrix().scale(1.3);

var a = Titanium.UI.createAnimation();
a.transform = t1;
a.duration = 600;

// when this animation completes, scale to normal size
a.addEventListener('complete', function()
{
    // we can use the identity transform to take it back to it's real size
  var t2 = Titanium.UI.create2DMatrix();
  view.animate({transform:t2, duration:200});
});

// Add to the parent view.
    view.add(okButton);
    view.add(cancelButton);
    view.add(titleLabel);
    view.add(texto);
    view.add(iv);

function anim(e){
    view.removeEventListener('postlayout', anim);
    view.animate(a);
}

view.addEventListener('postlayout', anim);

return view;
}

module.exports = customAlertJuegoPorEnviar;