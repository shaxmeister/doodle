
///	Crear Alerta personalizada	///
function customAlertSolicitudAmigo(params)
{
    
    //////// ws responder Sol Amistad   //////
    function respSolAm(mode){
        
        Ti.API.info("Responder Solicitud: \n num_tipo: "+mode+"\n ID__Invitacion: "+params.idInv+" \n  ID__UsuarioAmigo: "+params.idUserAmigo);
        
        var client = Ti.Network.createHTTPClient({
     // Procesamos la info
     onload : function(e) {
         Ti.API.info("customAlertSolicitudAmigo Received text: " + this.responseText);
         var res = JSON.parse(this.responseText);
         alert(res.mensaje);
     },
     onerror : function(e) {
         Ti.API.debug(e.error);
         alert('error');
     },
     timeout : 5000 
 });
 client.open("POST", params.url);
 client.send({
     'Num_Tipo' : (params.title=='Invitación Rechazada' ? 2 : mode),
     'ID__Invitacion':params.idInv,
     'ID__Usuario':params.ppid,
     'ID__UsuarioAmigo':params.idUserAmigo
 }); 
    }
    
    
	// Create a Button.
	var okButton = Ti.UI.createButton({
		title : '   Aceptar',
		backgroundImage:'Images/BtnAceptar.png',
		height : 36,
		color:'#fff',
		font:{
			fontSize:14,
			fontWeight:'normal'
		},
		width : 110,
		left : 14,
		bottom: 15
	});
	
	// Listen for click events.
	okButton.addEventListener('click', function() {
		respSolAm(3);
		params.hide();
	});
	
	
	// Create a Button.
	var cancelButton = Ti.UI.createButton({
		backgroundImage:'Images/BtnRechazar.png',
		title : '     Rechazar',
		color:'#fff',
		font:{
			fontSize:14,
			fontWeight:'normal'
		},
		height : 36,
		width : 110,
		right : 14,
		bottom: 15
	});
	
	// Listen for click events.
	cancelButton.addEventListener('click', function() {
	    respSolAm(4);
		params.hide();
	});
	
	
	// Create a Label.
	var titleLabel = Ti.UI.createLabel({
		text : params.title,
		color : 'black',
		font : {fontSize:18},
		width : Ti.UI.SIZE,
		top : 20,
		left : 65,
		textAlign : 'center'
	});
	
	// Create a Label.
	var texto = Ti.UI.createLabel({
		text : (''+params.user+' '+params.content),
		color: '#000',
		font : {fontSize:15},
		width : Ti.UI.SIZE,
		top : 54,
		left : 10,
		textAlign : 'center'
	});
	
	
	// Create an ImageView.
	var iv = Ti.UI.createImageView({
		image : 'Iconos/IconAddContact.png',
		width : 31,
		height : 36,
		top : 15,
		left : 23
	});
	


	
	var t = Titanium.UI.create2DMatrix().scale(0);
	var view = Titanium.UI.createView({
	backgroundImage:'Images/FondoAlert.png',
  	borderWidth:3,
  	borderColor:'#fff',
 	height:150,
 	width:260,
  	borderRadius:20,
	opacity:0.92,
	transform:t
	});


// create first transform to go beyond normal size
var t1 = Titanium.UI.create2DMatrix().scale(1.3);

var a = Titanium.UI.createAnimation();
a.transform = t1;
a.duration = 600;

// when this animation completes, scale to normal size
a.addEventListener('complete', function()
{
    // we can use the identity transform to take it back to it's real size
  var t2 = Titanium.UI.create2DMatrix();
  view.animate({transform:t2, duration:200});
});

// Add to the parent view.
	view.add(okButton);
	view.add(cancelButton);
	view.add(titleLabel);
	view.add(texto);
	view.add(iv);

function anim(e){
	view.removeEventListener('postlayout', anim);
	view.animate(a);
}

view.addEventListener('postlayout', anim);

return view;
}

module.exports = customAlertSolicitudAmigo;