var CustomRow = function(/*Object*/ params) {
	
	var row = Ti.UI.createTableViewRow({
		backgroundImage:'Images/off_2.png',
		selectedBackgroundImage:'Images/on_2.png',
		backgroundColor:'transparent',
		color:'transparent',
		selectedColor : 'transparent',
		height:40,
		className:'tablaJJ'
	});
	
	var rowImage = Ti.UI.createImageView({
		image:params.image,
		height:36,
		width:36,
		top:2,
		left:2,
	});
	row.add(rowImage);
	
	var primaryLabel = Ti.UI.createLabel({
		text:params.title,
		font:{
			fontSize:16,
			fontWeight:'normal'
		},
		color:'#003333',
		left:45
	});
	row.add(primaryLabel);
	
	
	return row;
};

module.exports = CustomRow;