
///	Crear Alerta personalizada	///
function customAlertEnvio()
{

	// Create a Button.
	var okButton = Ti.UI.createButton({
		title : '   Ok',
		backgroundImage:'Images/BtnAceptar.png',
		height : 36,
		color:'#fff',
		font:{
			fontSize:14,
			fontWeight:'normal'
		},
		width : 110,
		left :75,
		bottom: 15
	});
	
	// Listen for click events.
	okButton.addEventListener('click', function() {
		view.hide();
	});
	
	// Create a Label.
	var titleLabel1 = Ti.UI.createLabel({
		text : 'Dibujo Enviado:',
		color : '#003333',
		font : {fontSize:17},
		width : Ti.UI.SIZE,
		top : 18,
		left : 70,
		textAlign : 'center'
	});
	
	// Create a Label.
	var titleLabel2 = Ti.UI.createLabel({
		text : '15 de Septiembre 2012',
		color : 'black',
		font : {fontSize:15},
		width : Ti.UI.SIZE,
		top : 48,
		left : 69,
		textAlign : 'center'
	});
	
	// Create a Label.
	var texto = Ti.UI.createLabel({
		text : '11:30 am',
		color: '#000',
		font : {fontSize:15},
		width : Ti.UI.SIZE,
		top : 76,
		left : 69,
		textAlign : 'center'
	});
	
	// Create an ImageView.
	var iv = Ti.UI.createImageView({
		image : 'Iconos/date.png',
		width : 31,
		height : 29,
		top : 48,
		left :  27
	});
	
	iv.addEventListener('load', function() {
		Ti.API.info('Image loaded!');
	});
	
	// Create an ImageView.
	var ic = Ti.UI.createImageView({
		image : 'Iconos/time.png',
		width : 22,
		height : 22,
		top : 76,
		left : 31
	});
	
	ic.addEventListener('load', function() {
		Ti.API.info('Image loaded!');
	});
	
	var t = Titanium.UI.create2DMatrix().scale(0);
	var view = Titanium.UI.createView({
	backgroundImage:'Images/FondoAlert.png',
  	borderWidth:3,
  	borderColor:'#fff',
 	height:150,
 	width:260,
  	borderRadius:20,
	opacity:0.92,
	transform:t
	});


// create first transform to go beyond normal size
var t1 = Titanium.UI.create2DMatrix().scale(1.3);

var a = Titanium.UI.createAnimation();
a.transform = t1;
a.duration = 600;

// when this animation completes, scale to normal size
a.addEventListener('complete', function()
{
    // we can use the identity transform to take it back to it's real size
  var t2 = Titanium.UI.create2DMatrix();
  view.animate({transform:t2, duration:200});
});

// Add to the parent view.
	view.add(okButton);
	view.add(ic);
	view.add(titleLabel1);
	view.add(titleLabel2);
	view.add(texto);
	view.add(iv);

function anim(e){
	view.removeEventListener('postlayout', anim);
	view.animate(a);
	Ti.API.info('postlayout')
}

view.addEventListener('postlayout', anim);

return view;
}

module.exports = customAlertEnvio;